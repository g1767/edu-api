const express = require('express');
const router = express.Router();
const {
    protect
} = require('../middleware/auth');

const {
    getQuiz
} = require('../controllers/Quiz');  

router.get('/spec',protect,getQuiz);

module.exports = router
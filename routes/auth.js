const express = require('express');
const router = express.Router();
const {
    protect
} = require('../middleware/auth');

const {
    Login,
    Register,
    Logout
} = require('../controllers/Auth');

router.get('/login', Login);
router.post('/register', Register);
router.get('/logout',protect, Logout);

module.exports = router;
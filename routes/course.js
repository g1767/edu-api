const express = require('express');
const router = express.Router();
const {
    protect
} = require('../middleware/auth');

const {
   getCourses,
   getCourse,
   filterCourse,
   getContent,
} = require('../controllers/Course');  

router.get('/all',getCourses);
router.get('/specific',protect,getCourse);
router.post('/filter',protect,filterCourse);
// router.get('/content',getContent);

module.exports = router


const express = require('express');
const router = express.Router();
const {
    protect
} = require('../middleware/auth');

const {
   getProfile,
   updateProfile,

} = require('../controllers/User');  

router.get('/',protect,getProfile);
router.post('/update', protect, updateProfile);

module.exports = router
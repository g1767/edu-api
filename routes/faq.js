const express = require('express');
const router = express.Router();

const {
   getFAQ 
} = require('../controllers/FAQ');  

router.get('/', getFAQ);

module.exports = router
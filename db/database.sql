-- create database eduspace;

-- User
create TABLE Users(
    id SERIAL PRIMARY KEY,
    name varchar(50),
    username varchar(50) NOT NULL,
    password varchar(50) NOT NULL,
    urlImage varchar(500),
);


-- FAQ
create TABLE FAQ(
    id SERIAL PRIMARY KEY,
    title varchar(100),
    content varchar(1000),
);

-- Corses
CREATE TABLE COURSES(
    id SERIAL PRIMARY KEY,
    title varchar(100),
    university varchar(100),
    users int default 0,
    subject varchar(100),
    thumbnail varchar(200),
);

INSERT into COURSES(title, university, users, subject, thumbnail) values
    ('Digital Engagement', 'Universitas Indonesia', 0,'Technology', 'https://cdn.discordapp.com/attachments/755605623214964900/923835788020564049/Digital.png'),
    ('Mathematics', 'Universitas Indonesia', 0,'Science' ,'https://cdn.discordapp.com/attachments/755605623214964900/923835788511281182/Math.jpeg'),
    ('Drawing', 'President University', 0,'Technology' ,'https://cdn.discordapp.com/attachments/755605623214964900/923835788272230400/Drawing.jpg'),
    ('Bahasa Inggris', 'Institut Teknologi Bandung', 0,'Technology', 'https://cdn.discordapp.com/attachments/755605623214964900/923835787768909874/Enggris.png'),
    


CREATE TABLE Video(
    id SERIAL PRIMARY KEY,
    idc varchar(100),
    url varchar(500),
    thumbnail varchar(500),
    FOREIGN KEY (idc) REFERENCES COURSES(id)
);

INSERT INTO Video(idc, url , thumbnail) values
    (1, 'https://www.youtube.com/watch?v=5-ciHIwzBCo','https://cdn.discordapp.com/attachments/755605623214964900/866935770806878228/mobile-app-1.png'),
    (1, 'https://www.youtube.com/watch?v=waQRM7N-Wvg', 'https://cdn.discordapp.com/attachments/755605623214964900/866126295480795156/investigation_1.png'),
    (2, 'https://www.youtube.com/watch?v=OmJ-4B-mS-Y', 'https://cdn.discordapp.com/attachments/755605623214964900/923835788511281182/Math.jpeg')
    (2, 'https://www.youtube.com/watch?v=5-ciHIwzBCo', 'https://cdn.discordapp.com/attachments/755605623214964900/923835788511281182/Math.jpeg')


CREATE TABLE ENROLMENT(
    id SERIAL PRIMARY KEY AUTOINCREMENT,
    id_user int,
    id_course int,
    FOREIGN KEY (id_user) REFERENCES user (id),
    FOREIGN KEY (id_course) REFERENCES COURSES(id)
);

INSERT into ENROLMENT(id_user, id_course) VALUES
    (1, 2),
    (1, 3);


CREATE TABLE Quiz(
    id SERIAL PRIMARY KEY,
    idc int,
    title varchar(255),
    FOREIGN KEY (idc) REFERENCES COURSES(id)
);

INSERT into Quiz(idc,title) VALUES
    (1,'Quiz Satu'),
    (1,'Quiz Dua'),
    (2,'Quiz Tiga'),
    (2,'Quiz Empat');

CREATE TABLE SOAL(
    id SERIAL PRIMARY KEY,
    id_quiz int,
    title varchar(100),
    ssatu varchar(500),
    sdua varchar(500),
    stiga varchar(500),
    sempat varchar(500),
    jawaban varchar(10),
    FOREIGN KEY (id_quiz) REFERENCES Quiz(id)
);

INSERT into SOAL(id_quiz, title, ssatu, sdua, stiga, sempat, jawaban) values
    (1, 'Apakah pisang buah ? ', 'ya' ,'tidak','tidak','tidak' ,'a'),
    (1, 'Apakah melon buah ? ', 'ya' ,'tidak','tidak','tidak' ,'a'),
    (1, 'Apakah cabe buah ? ', 'ya' ,'tidak','tidak','tidak' ,'b'),
    (1, 'Apakah kamu buah ? ', 'ya' ,'tidak','tidak','tidak' ,'c'),
    (2, 'Apakah pisang buah ? ', 'ya' ,'tidak','tidak','tidak' ,'a'),
    (2, 'Apakah melon buah ? ', 'ya' ,'tidak','tidak','tidak' ,'a'),
    (2, 'Apakah cabe buah ? ', 'ya' ,'tidak','tidak','tidak' ,'b'),
    (2, 'Apakah kamu buah ? ', 'ya' ,'tidak','tidak','tidak' ,'c');
    







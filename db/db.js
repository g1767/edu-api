const Pool = require('pg').Pool;
require("dotenv").config();

// const devConfig = `postgresql://${process.env.PG_USER}:${process.env.PG_PASSWORD}@${process.env.PG_HOST}:${process.env.PG_PORT}/${process.env.PG_DATABASE}`;

const devConfig = {
    user : 'postgres', 
    password : '2002',
    port : 5432,
    database : 'eduspace'
};

const proConfig = 'postgres://opwacuinyabmtf:eae5b7c86aef0264cbf0c2ea1cf7f7f2d6accf21392439279a4fd27e8f26e87b@ec2-52-70-205-234.compute-1.amazonaws.com:5432/deshh8a0b34q7e'; //heroku addons

// const herokuConfig = {
//     user : 'opwacuinyabmtf',
//     password : 'eae5b7c86aef0264cbf0c2ea1cf7f7f2d6accf21392439279a4fd27e8f26e87b',
//     port : 5432,
//     database : 'deshh8a0b34q7e',
//     host :'ec2-52-70-205-234.compute-1.amazonaws.com'
// }

// const psql = process.env.NODE_ENV === "production" ? new Pool({
//     connectionString: proConfig,
// }) : new Pool(devConfig)

const psql = new Pool({ connectionString: proConfig, ssl: {
    rejectUnauthorized: false
  }})

psql.connect().then( () => {
    console.log('connected')
  }).catch(e=> {
      console.log('ERROR')
    console.log(e)
  })

// const psql = new Pool(herokuConfig)
module.exports = psql;  
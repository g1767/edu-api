const jwt = require('jsonwebtoken');
const asyncHandler = require('./async');
const ErrorResponse = require('../utils/errorResponse');
const psql = require('../db/db');


// Protect routes
exports.protect = asyncHandler(async (req, res, next) => {
    let token;
  
    if (
      req.headers.authorization &&
      req.headers.authorization.startsWith('Bearer')
    ) {
      // Set token from Bearer token in header
      token = req.headers.authorization.split(' ')[1];
      // Set token from cookie
    }
    
    // Make sure token exists
    if (!token) {
      req.user = null;
    }else{
      const {rows} = await psql.query(`
        SELECT * from users where auth = '${token}';
      `)
      
      const relatedUser = rows[0];
      req.user = relatedUser;
    }

    next();
});
  
  
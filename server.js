const path = require('path');
const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');
const xss = require('xss-clean');
const cors = require('cors');
const hpp = require('hpp');
const fileupload = require('express-fileupload');


// Load env vars
dotenv.config();


const app = express();

// Dev logging middleware
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}


// Set security headers
app.use(helmet());

// Body parser
app.use(express.json());

// Cookie parser
app.use(cookieParser());

// Prevent XSS attacks
app.use(xss());

// Enable CORS
app.use(cors());

// Prevent http param pollution
app.use(hpp());

// File uploading
app.use(fileupload());


// Route files
const faq = require('./routes/faq')
const auth = require('./routes/auth')
const user  = require('./routes/user')
const course = require('./routes/course')
const quiz = require('./routes/quiz')

// Mounting route
app.use('/api/faq',faq)
app.use('/api/auth',auth)
app.use('/api/user',user)
app.use('/api/course',course)
app.use('/api/quiz',quiz)


const PORT = process.env.PORT || 5000;

const server = app.listen(
    PORT,
    console.log(
      `Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold
    )
);
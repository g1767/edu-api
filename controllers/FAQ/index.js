const psql = require('../../db/db')

exports.getFAQ = async(req,res,next)=>{
    try{
        const GET_FAQ = await psql.query(`
            SELECT * from faq;
        `)

        const allFAQ = GET_FAQ.rows;

        console.log(allFAQ)

        res.status(200).json({
            res : 'OK',
            data : allFAQ,
        })
    }catch(e){
        console.log(e)
        res.status(500).json({
            res : 'Error',
            message : e
        })
    }
}
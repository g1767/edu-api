const psql = require('../../db/db')
const {
    userCourses,
    userProfile,
    updateProfile,
} = require('./SQL')

exports.getProfile = async(req,res)=>{
    const user = req.user;

    if(user){
        let {rows} = await psql.query(userCourses(user.id));
        const courses = rows;
        let profile = await psql.query(userProfile(user.id));
        profile = profile.rows;

        res.status(200).json({
            res : 'Success',
            profile : profile,
            courses : rows,
        })
    }else{
        res.status(401).json({
            status : 'Not Auhorized'
        })
    }

}

exports.updateProfile = async(req,res,next)=>{
    const user = req.user;
    const {name, username, password} = req.body;
    if(user ){
        let thumbnail = await updatePicture();
        let rows = await psql.query(updateProfile(user.id, name, username, password,thumbnail));
        if(name && username && password){
            res.status(200).json({
                res : 'Success',
                message : 'Your profile have been updated'
            })
        }else{
            res.status(400).json({
                res : 'Failed',
                message : 'Please input name, username, and password'
            })
        }
    }else{
        res.status(401).json({
            status : 'Not Auhorized'
        })
    }
}

const updatePicture = async()=>{
    return 'https://cdn0-production-images-kly.akamaized.net/OnqFJCg4PoF-4YKPHZAeNlH_J90=/1280x720/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/3167999/original/084845700_1593662619-Template_Landscape_1_.jpg'
}
const psql = require('../../db/db')
const {
    AllCourse,
    SpecificVideos,
    AllVideos,
    FilterCourses,
    SpecificCourse,
} = require('./SQL')

exports.getCourses = async(req,res,next) => {
    let {rows} = await psql.query(AllCourse());
        
    res.status(200).json({
        res : 'Success',
        data : rows
    })
}


const isEnroll = async(user, id_course)=>{
    const user_id = user.id;
    let {rows} = await psql.query(`
        SELECT from enrolment 
        where id_user = '${user_id}' AND
        id_course  = '${id_course}';
    `)

    let lst = rows.length;
    
    return lst !== 0;
}

exports.getCourse = async(req,res,next) => {

    const {user} = req;
    const {id} = req.query

    if(user){
        let {rows} = await psql.query(SpecificCourse(id))
        let course = rows[0];
        
        let video = await psql.query(AllVideos(id))
        video = video.rows

        const enrolment = await isEnroll(user, id);

        res.status(200).json({
            res : 'Success',
            course : course,
            videos : video,
            isEnroll : enrolment,
        })
    }else{
        res.status(401).json({
            res : 'Failed',
            message : 'Unauthorized'
        })
    }

}

// Tags : Technology, Mathematics, Art, Science 
exports.filterCourse = async(req,res,next) => {
    
    const {user} = req;
    const {subject, university} = req.query;


    if(user){

        let {rows} = await psql.query(AllCourse());

        if(subject == '' && university == ''){
            res.status(200).json({
                res : 'Success',
                data : rows
            })
        }else{

            let courses = rows;

            if(subject !== undefined)
                courses = courses.filter(course => course.subject == subject);
            
            console.log(courses)
            if(university !== undefined)
                courses = courses.filter(course => course.university == university);

            res.status(200).json({
                res : 'success',
                data : courses
            })

        }   

    }else{
        res.status(401).json({
            res : 'Failed',
            message : 'Unauthorized'
        })
    }
    
}

exports.getContent = async(req,res,next) => {

}




const GetCourses = ()=>{
    return `
        SELECT * from courses;
    `
}

const GetCourse = (id)=>{
    return `
    SELECT * from courses
    where id = '${id}';
    `
}

const FilterCourses = (subject)=>{
    return `
    SELECT * from courses
    where subject = '${subject}';
    `
}

const GetAllVideos = (id_course)=>{
    return `
    SELECT * from video
    where idc= '${id_course}'
    `
}

const GetSpecificVideos = (id_video)=>{
    return `
    SELECT * from video
    where id = '${id_video}';
    `
}

module.exports = {
    AllCourse : GetCourses,
    SpecificCourse : GetCourse,
    FilterCourses : FilterCourses,
    AllVideos : GetAllVideos,
    SpecificVideos : GetSpecificVideos
}
const psql = require('../../db/db')
const {
    findOneUser,
    authUser,
    removeAuth,
} = require('./SQL');
const jwtGenerator = require('../../utils/jwtGenerator');

exports.Register = async(req,res,next)=>{

    const urlQuery = req.query;

    try{
        const INSERT_USER = await psql.query(`
            INSERT INTO Users(name,username,password) values
                ('${urlQuery.name}', '${urlQuery.username}', '${urlQuery.password}');
        `)

        res.status(200).json({
            res : 'OK',
            message : 'Your account has been made'
        })
        
    }catch(e){
       
        if(urlQuery.name !== undefined && urlQuery.username !== undefined && urlQuery.password !== undefined){
            res.status(403).json({
                res : 'Error',
                message : 'Account is have been made'
            })    
        }else{
            res.status(405).json({
                res : 'Error',
                message : 'Please fill all the forms'
            })
        }

    }
}

exports.Login = async(req,res,next)=>{

    const { username, password} = req.query;

    
    try{
        if( !username || !password)
        res.status(500).json({
            res : 'Error',
            message : 'Please fill all forms'
        })
    
        const {rows} = await psql.query(findOneUser(username));
        const User = rows[0];
        const token = jwtGenerator(User.id);
        const auth = await psql.query(authUser(username, token));
        
        res.status(200).json({
            status : 'Success',
            token : token
         })
    }catch(e){
        console.log(e)
        res.status(500).json({
            response : 'Error',
            message : 'Account is not found',
        })
    }

}

exports.Logout = async(req,res,next)=>{
    
    const {user} = req;

    if(user){
        const {rows} = await psql.query(removeAuth(user.id));
        
        res.status(200).json({
            response : 'Success',
            message : 'Account have been logout'
        })
    }else{
        res.status(500).json({
            response : 'Error',
            message : 'Account is not found',
        })
    }

}


